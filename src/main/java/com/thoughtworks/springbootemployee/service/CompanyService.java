package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;

public interface CompanyService {
    List<Company> getCompanyList();

    Company getCompanyById(int id);

    List<Employee> getEmployeesByCompanyId(int id);

    int createCompany(Company company);

    List<Company> getCompanyListByPage(int page, int size);

    Company updateCompany(int id, Company company);

    int deleteCompany(int id);
}
