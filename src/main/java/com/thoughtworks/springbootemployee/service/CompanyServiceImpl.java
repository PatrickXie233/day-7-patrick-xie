package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class CompanyServiceImpl implements CompanyService {
    private final EmployeeRepository employeeRepository;
    private final CompanyRepository companyRepository;

    public CompanyServiceImpl(EmployeeRepository employeeRepository, CompanyRepository companyRepository) {
        this.employeeRepository = employeeRepository;
        this.companyRepository = companyRepository;
    }

    @Override
    public List<Company> getCompanyList() {
        return companyRepository.getCompanyList();
    }

    @Override
    public Company getCompanyById(int id) {
        return companyRepository.getCompanyById(id);
    }

    @Override
    public List<Employee> getEmployeesByCompanyId(int id) {
        return employeeRepository.getEmployeeList().stream()
                .filter(employee -> employee.getCompanyId() == id)
                .collect(Collectors.toList());
    }

    @Override
    public int createCompany(Company company) {
        return companyRepository.createCompany(company);
    }

    @Override
    public List<Company> getCompanyListByPage(int page, int size) {
        return companyRepository.getCompanyListByPage(page, size);
    }

    @Override
    public Company updateCompany(int id, Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @Override
    public int deleteCompany(int id) {
        int deleteId = companyRepository.deleteCompany(id);
        employeeRepository.getEmployeeList().stream()
                .filter(employee1 -> employee1.getCompanyId() == deleteId)
                .collect(Collectors.toList())
                .forEach(employee -> {
                    employeeRepository.getEmployeeList().remove(employee);
                });
        return deleteId;
    }
}
